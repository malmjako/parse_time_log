"""
This program parses a time log file in the following format:

Fri Nov 6 08:07:48 CET 2009 in
Fri Nov 6 15:12:16 CET 2009 hem
Mon Nov 9 09:14:10 CET 2009 in
Mon Nov 9 18:13:49 CET 2009 hem
Tue Nov 10 08:06:49 CET 2009 in
Tue Nov 10 18:08:24 CET 2009 hem
Wed Nov 11 08:12:28 CET 2009 in
Wed Nov 11 11:48:32 CET 2009 lunch 1.5h
Wed Nov 11 19:54:33 CET 2009 hem
Thu Nov 12 08:17:26 CET 2009 in
Thu Nov 12 11:50:54 CET 2009 lunch
Thu Nov 12 12:40:00 CET 2009 in
Thu Nov 12 15:20:00 CET 2009 innebandy
Thu Nov 12 16:30:00 CET 2009 in
Thu Nov 12 19:17:07 CET 2009 hem
Fri Nov 13 08:00:00 CET 2009 ledig

The records (lines) can be created by running::

    echo `date` hem >> ~/time_log

I have the following small script in ~/bin/event::

    #!/bin/bash
    #
    # Time logger
    
    eventstr="`date` $@"
    echo $eventstr >> ~/time_log
    echo $eventstr

30 min is subtracted for lunch on days when no lunch time was specified. The
total working hours are then calculated for each week in the period.

"""

import datetime as datetime_module

class datetime(datetime_module.datetime):
    def hours(self):
        return self.hour + self.minute/60. + self.second/3600.

class timedelta(datetime_module.timedelta):
    def hours(self):
        return self.days*24 + self.seconds/3600.
    
    @staticmethod
    def from_td(td):
        return timedelta(td.days, td.seconds, td.microseconds)


import os
TIME_LOG_FILE = os.path.join(os.environ['HOME'], 'time_log')
PLOT_FILE = os.path.join(os.environ['HOME'], 'time_log.png')
DEFAULT_LUNCH_TIME = datetime_module.time(11,30)
DEFAULT_LUNCH_DURATION = timedelta(seconds=30*60) # Default 30 min lunch
MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
FULL_WEEK = timedelta(seconds=40*3600) # Full work week, 40 h


def parse_duration(duration):
    import re
    match = re.match('([0-9\.]+)(h|m)', duration)
    if match:
        value, unit = match.groups()
        assert unit in ['h', 'm']
        factor = 60 * 60 if unit == 'h' else 60
        return timedelta(0, float(value) * factor)
    else:
        raise ValueError("Duration %r could not be parsed" % duration)


class Absence:
    """Stores an absence event"""
    def __init__(self, title, start, stop = None):
        self.start = start
        self.stop = stop
        
        try:
            title, duration_s = title.split()
        except ValueError:
            pass
        else:
            self.stop = self.start + parse_duration(duration_s)
        
        self.title = title
    
    def duration(self):
        try:
            return timedelta.from_td(self.stop - self.start)
        except:
            raise Exception('Couldn\'t calculate duration of %s' % self.title)
    
    def __repr__(self):
        return '%s: %s, %s - %s' % (self.title, 
                                    self.start.date(),
                                    self.start.time(),
                                    self.stop.time())

class WorkingDay:
    """Takes care of a day's events"""
    def __init__(self, dt, title='in'):
        self.start = dt
        self.absence = []
        self.current_absence = None
        self.stop = None
        self.time_off = timedelta(0)
        
        if title == 'in':
            pass
        elif title.startswith('ledig'):
            self.add_time_off(dt, title)
            self.stop = dt # Stop this day
        else:
            raise ValueError("Can't initialize a day with event %s. Only 'in' "
                             "or 'ledig' are allowed." % title)
    
    def handle_event(self, dt, title):
        """Handle event"""
        if self.current_absence == None: # Either a new
            if title == 'hem':
                self.stop = dt
            elif title.startswith('ledig'):
                self.add_time_off(dt, title)
            else:
                a = Absence(title, dt)
                self.absence.append(a)
                if not a.stop:
                    self.current_absence = a
        elif title == 'in':
            self.current_absence.stop = dt
            self.current_absence = None
        else:
            raise Exception('Unhandled event "%s" (%r)' % (title, self))
    
    def add_time_off(self, dt, title):
        if title == 'ledig':
            self.time_off = timedelta.from_td(FULL_WEEK / 5)
        else:
            ledig, duration_s = title.split() #@UnusedVariable
            self.time_off += parse_duration(duration_s)
        
        if self.time_off >= FULL_WEEK / 5:
            # Don't subtract lunch when summing day
            self.absence.append(Absence('lunch', dt, stop=dt)) # quick lunch ;-)
    
    def get_datetime(self, time):
        """Utility function"""
        return datetime(self.start.year, self.start.month, self.start.day,
                                 time.hour, time.minute, time.second)
    
    def total(self):
        """Return total work hours this day"""
        tot = self.duration() - DEFAULT_LUNCH_DURATION # Subtract lunch, add it back if specified 
        for a in self.absence:
            tot = tot - a.duration()
            if a.title.lower() == 'lunch':
                tot = tot + DEFAULT_LUNCH_DURATION
        
        return timedelta.from_td(tot)
    
    def duration(self):
        """
        Return total duration of work day, from start until stop plus time off.
        
        """
        td = self.time_off
        
        if self.stop == None and self.start.date() == datetime_module.date.today():
            # Calculate duration up to now
            td += datetime.now() - self.start
        else:
            td += self.stop - self.start
        return timedelta.from_td(td)
    
    def get_absence(self):
        absence_list = list(self.absence) # Copy list
        if not 'lunch' in [a.title for a in self.absence]:
            absence_list.append(Absence('lunch', 
                                        self.get_datetime(DEFAULT_LUNCH_TIME),
                                        self.get_datetime(DEFAULT_LUNCH_TIME)+DEFAULT_LUNCH_DURATION))
        return absence_list
    
    def __str__(self):
        if self.start == self.stop:
            if self.time_off > timedelta(0):
                return "%s -- ledig" % self.start.date()
        
        s = '%s - ' % self.start
        if self.start.date() == self.stop.date():
            s += str(self.stop.time())
        else:
            s += str(self.stop)
        if len(self.absence) > 0:
            for a in self.absence:
                s += '\n\t%s %s - %s' % (a.title, a.start.time(), a.stop.time())
        return s
    
    def __repr__(self):
        args = repr(self.start.date())
        if self.time_off >= FULL_WEEK / 5:
            args += ", ledig"
        return "Day(%s)" % args


class WorkingWeek:
    """Takes care of a week's working days"""
    def __init__(self):
        self.days = dict()
    
    def add(self, day):
        wd = day.start.weekday()
        self.days[wd] = day
    
    def get_day(self, dt):
        """Return the WorkingDay object for the specified date"""
        wd = dt.weekday()
        if wd in self.days:
            return self.days[wd]
        else:
            #Create a new WorkingDay object from date
            self.days[wd] = WorkingDay(dt)
        return self.days[wd]
    
    def sorted(self):
        dlist = self.days.items()
        dlist.sort()
        return dlist

    
    def total(self, balance=False):
        if balance == True:
            tot = -FULL_WEEK # Measure time relative to full working week
        else:
            tot = timedelta() # Zero time duration
        for d in self.days.itervalues():
            tot = tot + d.total()
        
        return timedelta.from_td(tot)
    
class WorkingPeriod:
    """Takes care of all the working weeks during the period of the time log"""
    def __init__(self):
        self.years = dict()
    
    
    def add(self, day):
        """Add working day to period."""
        iso_year, iso_week, iso_weekday = day.start.isocalendar()
        self.get_week(iso_year, iso_week).add(day)
    
    
    def get_week(self, year, week_number):
        """Get the WorkingWeek specified"""
        if not year in self.years:
            self.years[year] = dict()
        if not week_number in self.years[year]:
            self.years[year][week_number] = WorkingWeek()
            
        return self.years[year][week_number]
    
    
    def sorted(self):
        ylist = self.years.items()
        ylist.sort()
        retlist = []
        for year, weeks in ylist:
            wlist = weeks.items()
            wlist.sort()
            retlist.append((year, wlist))
        return retlist
       
        
    def total(self, balance=False):
        tot = timedelta() # Zero time duration
        for year, weeks in self.years.iteritems():
            for week_number, week in weeks.iteritems():
                tot = tot + week.total(balance)
        
        return timedelta.from_td(tot)
    
    def plot(self, save=False, start=None, stop=None):
        """Present the period graphically"""
        import matplotlib
        if save == True:
            matplotlib.use('Agg')
        import pylab
        
        # TODO: Add plot showing saldo
        #dates = []
        #saldo = []
        
        for year, weeks in self.years.iteritems():
            for week_number, week in weeks.iteritems():
                for day in week.days.itervalues():
                    if day.vacation == True:
                        colour = 'yellow'
                    else:
                        colour = 'green'
                    pylab.bar(day.start.date(), 
                              day.duration().hours(), 
                              bottom=day.start.hours(), 
                              color=colour)
                    
                    for a in day.get_absence():
                        pylab.bar(day.start.date(), 
                                  a.duration().hours(), 
                                  bottom=a.start.hours(), 
                                  color='red')
        
        if save == True:
            pylab.savefig(PLOT_FILE)
            os.system('gnome-open %s' % PLOT_FILE)
            

def parse_file(path=TIME_LOG_FILE):
    """Parses the time log file, returns a WorkingPeriod"""
    period = WorkingPeriod()
    workday = None
    
    f = open(path, 'r')
    
    for line in f.readlines():
        weekday, month, day, timestr, timezone, year = line.split()[:6]
        title = ' '.join(line.split()[6:])
        
        hour, minute, second = [int(t) for t in timestr.split(':')]
        dt = datetime(int(year), 
                         MONTHS.index(month)+1, 
                         int(day),
                         hour,
                         minute,
                         second)
        
        if workday is None:
            workday = WorkingDay(dt, title)
            period.add(workday)
        else:
            workday.handle_event(dt, title)
        if workday.stop:
            # Finished processing workday
            print(workday)
            workday = None
    
    return period


def total(period):
    """Print total worked hours in the period""" 
    period_balance = 0
    for year, weeks in period.sorted():
        for week_number, week in weeks:
            tot = week.total().hours()
            bal = week.total(balance=True).hours()
            period_balance += bal
            if bal > 0:
                pm = '+'
            else:
                pm = ''
            if period_balance > 0:
                period_pm = '+'
            else:
                period_pm = ''
            print(('%d, v %d:\t%.1f h (%s%.1f h)  \tBalance: %s%.1f h') % 
                  (year, week_number, tot, pm, bal, period_pm, period_balance))
    tot = period.total().hours()
    bal = period.total(balance=True).hours()
    if bal > 0:
        pm = '+'
    else:
        pm = ''
    print(('Total for period: %.1f h ('+pm+'%.1f h)\n') % (tot, bal))

def report(period, weeks=None):
    """Print total worked hours for each day of each week in the period or of
    the specified weeks (not yet implemented)."""
    def hours(td):
        return td.days * 24 + td.seconds / 60. / 60.
    
    if weeks == None:
        for year, weeks in period.sorted():
            for week_number, week in weeks:
                print('%d, v %d:' % (year, week_number))
                for date, day in week.sorted():
                    if day.time_off:
                        print('%s: %s (ledig %.1f h)' % (day.start.date(),
                                                         day.total(),
                                                         hours(day.time_off)))
                    else:
                        print('%s: %s' % (day.start.date(), day.total()))
                tot = week.total()
                print('Total: %.1f h\n' % (tot.hours()))
    else:
        return # TODO: Implement report for specific weeks

if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-p', '--plot', action='store_true',
                      help="Plot worked hours")
    
    opts, args = parser.parse_args()
    p = parse_file()
    report(p)
    total(p)
    if opts.plot:
        p.plot(save=True)
